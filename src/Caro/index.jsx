import React from "react";
import Caro from "./lib/tic-tac-toe.js";
import "./index.less";
function Square(props) {
  return (
    <button
      className={props.but}
      onClick={props.onClick} /*disabled={props.isOver}*/
    >
      {props.value}
    </button>
  );
}
/*class Square extends React.Component{
    render(){
      return(
        <button className={this.props.but} onClick={this.props.onClick} disabled={this.props.ending}>
            {this.props.value}
        </button>
      );
    }
  }*/
class Board extends React.Component {
  test()
  {
    alert("hello");
  }
  render() {
    let board = this.props.state.board;
    let vis = this.props.state.vis;
    let xIsNext = this.props.state.xIsNext;
    let undomove = this.props.state.undomove;
    let stepNumber = this.props.state.stepNumber;
    let a=this.props.state.a,b=this.props.state.b,c=this.props.state.c,d=this.props.state.d,e=this.props.state.e;
    const array = Array(20).fill(null);
    for (let i = 0; i < 20; i++) {
      const subarray = Array(20).fill(null);
      for (let j = 0; j < 20; j++) {
        var squarekey=i*20+j;
        subarray.push(
          <span key={squarekey}>
          <button
            className={
              ((i * 20 + j == this.props.state.a ||
                i * 20 + j == this.props.state.b ||
                i * 20 + j == this.props.state.c ||
                i * 20 + j == this.props.state.d ||
                i * 20 + j == this.props.state.e) &&
              this.props.isEnding)
              
                ? "red"
                : "square"
            }
            onClick={() => {this.props.tick({ x: squarekey})}}
            //sOver={this.props.isEnding}
          >{this.props.state.board[squarekey]}
          </button>
          </span>
        );
      }
      array.push(
        <div className="board-row" key={i}>
          {subarray}
        </div>
      );
    }
    return (
      <div>
        <button onClick={this.props.undo}>Undo</button>
        {array}
      </div>
    );
  }
}
export default Board;
