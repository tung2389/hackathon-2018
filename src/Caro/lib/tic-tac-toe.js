"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

const Caro = {
    default(props = {n:20,m:20})
    {
        let board=Array(400).fill(null);
        let vis=Array(400).fill(0);
        let xIsNext=true;
        let stepNumber=0;
        let undomove=Array(400).fill(0);
        let a=null,b=null,c=null,d=null,e=null;
        return {board,xIsNext,vis,stepNumber,undomove,a,b,c,d,e};
    },
    action:
    {
    async tick(state, {x}){
        let vis=state.vis;
        let stepNumber=state.stepNumber;
        let board=state.board;
        let xIsNext=state.xIsNext;
        let undomove=state.undomove;
        let a=state.a,b=state.b,c=state.c,d=state.d,e=state.e;
        //if(vis[x]===0)
        //{
        if(xIsNext)
        {
            board[x]="X";
        }
        else
        {
            board[x]="O";
        }
        vis[x]=1;
        xIsNext=!xIsNext;
        undomove[stepNumber]=x;
        stepNumber=stepNumber+1;
        //}
        return {board,xIsNext,vis,stepNumber,undomove,a,b,c,d,e};
    },
    async undo(state){
        let vis=state.vis;
        let stepNumber=state.stepNumber;
        let board=state.board;
        let xIsNext=state.xIsNext;
        let undomove=state.undomove;
        let a=state.a,b=state.b,c=state.c,d=state.d,e=state.e;
        board[undomove[stepNumber-1]]=null;
        vis[undomove[stepNumber-1]]=0;
        stepNumber=stepNumber-1;
        xIsNext=!xIsNext;
        return {board,xIsNext,vis,stepNumber,undomove,a,b,c,d,e};
    }
},
    isValid(state) {
    },
    isEnding(state){
    let vis=state.vis;
    let stepNumber=state.stepNumber;
    let board=state.board;
    let xIsNext=state.xIsNext;
    let undomove=state.undomove;
    let a=state.a,b=state.b,c=state.c,d=state.d,e=state.e;
    for(let i=0;i<400;i++)
    {
        if(i<400-84 && board[i]==board[i+21] && board[i+21]==board[i+42] && board[i+42]==board[i+63] && board[i+63]==board[i+84] && board[i]!=null||
           i<400-80 && board[i]==board[i+20] && board[i+20]==board[i+40] && board[i+40]==board[i+60] && board[i+60]==board[i+80] && board[i]!=null||
           i<400-4 && board[i]==board[i+1] &&board[i+1]==board[i+2] && board[i+2]==board[i+3] && board[i+3]==board[i+4] && board[i]!=null||
           i<400-76 && board[i]==board[i+19] && board[i+19]==board[i+38] && board[i+38]==board[i+57] && board[i+57]==board[i+76] && board[i]!=null
        )
        {
          //this.setState({win:(this.state.xIsNext==true)?"O":"X",disable:true});
          if(i<400-84 && board[i]==board[i+21] && board[i+21]==board[i+42] && board[i+42]==board[i+63] && board[i+63]==board[i+84] && board[i]!=null)
          a=i,b=i+21,c=i+42,d=i+63,e=i+84;
          else if(i<400-80 && board[i]==board[i+20] && board[i+20]==board[i+40] && board[i+40]==board[i+60] && board[i+60]==board[i+80] && board[i]!=null)
          a=i,b=i+20,c=i+40,d=i+60,e=i+80;
          else if(i<400-4 && board[i]==board[i+1] &&board[i+1]==board[i+2] && board[i+2]==board[i+3] && board[i+3]==board[i+4] && board[i]!=null)
          a=i,b=i+1,c=i+2,d=i+3,e=i+4;
          else if(i<400-76 && board[i]==board[i+19] && board[i+19]==board[i+38] && board[i+38]==board[i+57] && board[i+57]==board[i+76] && board[i]!=null)
          a=i,b=i+19,c=i+38,d=i+57,e=i+76;
          if(xIsNext)
          return "O win";
          else
          return "X win";
        }
    }
    }
};
exports.default = Caro;