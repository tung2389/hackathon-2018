import React from 'react';
import Getsum from './lib/Sum.js';
import './index.less';
function Inp(props)
{
    return(
        <input className="inp" onChange={e => props.change(e)} />
    );
}
function Square(props)
{
    return(
        <button className="inp2">{props.value}</button>
    )
}
class Board extends React.Component{
    render()
    {
        let board=this.props.state.board;
        let show=this.props.state.show;
        let sumcol=this.props.state.sumcol;
        let sumrow=this.props.state.sumrow;
        let sumcoll=this.props.state.sumcoll;
        let sumroww=this.props.state.sumroww;
        let isBut=this.props.state.isBut;
        const n=this.props.state.n;
        const m=this.props.state.m;
        const array = [];
        for(let i=0;i<n;i++)
        {
            let subarray = [];
            for(let j=0;j<m;j++)
            {
                if(this.props.state.isBut[i][j]==1)
                {
                    subarray.push(
                        <Square 
                        value={this.props.state.board[i][j]}
                        />
                    );
                }
                else
                {
                    subarray.push(
                        <Inp
                        change={e => this.props.scan(e.target.value,i,j)}
                        />
                    )
                }
            }
            subarray.push(<h1>{this.props.state.sumrow[i]}</h1>);
            subarray.push(<h1>{this.props.state.sumroww[i]}</h1>);
            array.push(<div key={i}>{subarray}</div>)
        }
        let subarray = [];
        for(let j=0;j<m;j++)
        {
            subarray.push(<h1>{this.props.state.sumcol[j]}</h1>);
        }
        array.push(<div>{subarray}</div>);
        let subarray2= [];
        for(let j=0;j<m;j++)
        {
            subarray2.push(<h1>{this.props.state.sumcoll[j]}</h1>);
        }
        array.push(<div>{subarray2}</div>);
        return(
            <div>
                <h1> hello </h1>
                {array}
            </div>
        );
    }
}
export default Board;


        