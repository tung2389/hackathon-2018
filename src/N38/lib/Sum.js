"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

function rand(min,max)
{
    let r=Math.random();
    return Math.floor(r*max+min);
}
const Getsum = {
    default(props = {n:5,m:5})
    {
        const n=props.n;
        const m=props.m;
        let board=Array(n).fill(0);
        let show=Array(n).fill(0);
        let sumrow=Array(n).fill(0);
        let sumcol=Array(m).fill(0);
        let sumroww=Array(n).fill(0);
        let sumcoll=Array(m).fill(0);
        let isBut=Array(n).fill(0);
        for(let i=0;i<n;i++)
        {
            board[i]=Array(m).fill(0);
            show[i]=Array(m).fill(0);
            isBut[i]=Array(m).fill(0);
        }
        for(let i=0;i<n;i++)
        {
            for(let j=0;j<m;j++)
            {
                board[i][j]=rand(1,25);
                show[i][j]=rand(0,1);
                if(show[i][j]==1)
                isBut[i][j]=1;
                sumrow[i]=sumrow[i]+board[i][j];
            }
        }
        for(let j=0;j<m;j++)
        {
            for(let i=0;i<n;i++)
            {
                sumcol[j]=sumcol[j]+board[i][j];
            }
        }
        return {board,show,sumrow,sumcol,sumroww,sumcoll,isBut};
    },
    action:
    {
        async scan(state,{value,x,y})
        {
            const n=state.n;
            const m=state.m;
            let board=state.board;
            let show=state.show;
            let sumrow=state.sumrow;
            let sumcol=state.sumcol;
            let sumroww=state.sumroww;
            let sumcoll=state.sumcoll;
            let isBut=state.isBut;
            show[x][y]=1;
            board[i][j]=value;
            for(let i=0;i<n;i++)
            {
                sumroww[i]=0;
            }
            for(let j=0;j<m;j++)
            {
                sumcoll[j]=0;
            }
            for(let i=0;i<n;i++)
            {
                for(let j=0;i<m;j++)
                {
                    if(show[i][j]==1)
                    {
                        sumroww[i]=sumroww[i]+board[i][j];
                    }
                }
            }
        for(let j=0;j<m;j++)
        {
            for(let i=0;i<n;i++)
            {
                if(show[i][j]==1)
                sumcoll[j]=sumcoll[j]+board[i][j];
            }
        }
        return {board,show,sumrow,sumcol,sumroww,sumcoll,isBut};
        }
    },
    isValid(state) {
        // nothing to do here ?
    },
    isEnding(state){
        let board=state.board;
        let show=state.show;
        let sumrow=state.sumrow;
        let sumcol=state.sumcol;
        let sumroww=state.sumroww;
        let sumcoll=state.sumcoll;
        let isBut=state.isBut;
        const n=state.n;
        const m=state.m;
        for(let i=0;i<n;i++)
        {
            if(sumroww[i]!=sumrow[i])
            return null;
        }
        for(let j=0;j<m;j++)
        {
            if(sumcoll[j]!=sumcol[j])
            return null;
        }
        return "win";
    } 
}
exports.default =  Getsum;