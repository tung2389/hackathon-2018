import React from "react";
import { storiesOf } from "@storybook/react";
import { number, withKnobs } from "@storybook/addon-knobs";
import Board from "../src/N38/index.jsx";
import Game from "../src/N38/lib/Sum.js";
import ReactGame from "react-gameboard/lib/component";
const N38 = ReactGame(Game);

storiesOf("N(38)", module)
  .add("Easy", () => (
    <N38 n={5} m={5}>
      <Board />
    </N38>
  ));