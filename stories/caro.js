import React from "react";
import { storiesOf } from "@storybook/react";
import { number, withKnobs } from "@storybook/addon-knobs";
import Board from "../src/Caro/index.jsx";
import Game from "../src/Caro/lib/tic-tac-toe.js";
import ReactGame from "react-gameboard/lib/component";

const Caro = ReactGame(Game);

storiesOf("Caro", module)
  .add("Easy", () => (
    <Caro n={20} m={20}>
      <Board />
    </Caro>
  ));